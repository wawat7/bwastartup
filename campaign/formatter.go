package campaign

import "strings"

type CampaignFormatter struct {
	ID int `json:"id"`
	UserID int `json:"user_id"`
	Name string `json:"name"`
	ShortDescription string `json:"short_description"`
	ImageURL string `json:"image_url"`
	GoalAmount int `json:"goal_amount"`
	CurrentAmount int `json:"current_amount"`
	Slug string `json:"slug"`
}

func FormatCampaign(campaign Campaign) CampaignFormatter {
	formatter := CampaignFormatter{
		ID: campaign.ID,
		UserID: campaign.UserID,
		Name: campaign.Name,
		ShortDescription: campaign.ShortDescription,
		ImageURL: "",
		GoalAmount: campaign.GoalAmount,
		CurrentAmount: campaign.CurrentAmount,
		Slug: campaign.Slug,
	}
	if len(campaign.CampaignImages) > 0 {
		formatter.ImageURL =  campaign.CampaignImages[0].FileName
	}

	return formatter
}

func FormatCampaigns(campaigns []Campaign) []CampaignFormatter {
	campaignsFormatter := []CampaignFormatter{}
	for _, campaign := range campaigns {
		campaignFormatter := FormatCampaign(campaign)
		campaignsFormatter = append(campaignsFormatter, campaignFormatter)
	}

	return campaignsFormatter
}

type CampaignDetailFormatter struct {
	ID int `json:"id"`
	Name string `json:"name"`
	ShortDescription string `json:"short_description"`
	Description string `json:"description"`
	ImageURL string `json:"image_url"`
	GoalAmount int `json:"goal_amount"`
	CurrentAmount int `json:"current_amount"`
	BackerCount int `json:"backer_count"`
	UserID int `json:"user_id"`
	Slug string `json:"slug"`
	Perks []string `json:"perks"`
	User CampaignUserFormatter `json:"user"`
	Images []CampaignImageFormatter `json:"images"`
}

type CampaignUserFormatter struct {
	Name string `json:"name"`
	ImageURL string `json:"image_url"`
}

type CampaignImageFormatter struct {
	ImageURL string `json:"image_url"`
	IsPrimary bool `json:"is_primary"`
}

func FormatCampaignDetail(campaign Campaign) CampaignDetailFormatter {
	formatter := CampaignDetailFormatter{}
	formatter.ID = campaign.ID
	formatter.Name = campaign.Name
	formatter.ShortDescription = campaign.ShortDescription
	formatter.Description = campaign.Description

	formatter.GoalAmount = campaign.GoalAmount
	formatter.CurrentAmount = campaign.CurrentAmount
	formatter.BackerCount = campaign.BackerCount
	formatter.UserID = campaign.UserID
	formatter.Slug = campaign.Slug

	var perks []string
	for _, perk := range strings.Split(campaign.Perks, ",") {
		perks = append(perks,strings.TrimSpace(perk))
	}
	formatter.Perks = perks
	//made wawat
	//formatter.User.Name = campaign.User.Name
	//formatter.User.ImageURL = campaign.User.AvatarFileName

	user := campaign.User

	formatterUser := CampaignUserFormatter{}
	formatterUser.Name = user.Name
	formatterUser.ImageURL = user.AvatarFileName

	formatter.User = formatterUser

	images := campaign.CampaignImages
	formatterImages := []CampaignImageFormatter{}


	for _, image := range images {
		formatterImage := CampaignImageFormatter{}
		formatterImage.ImageURL = image.FileName

		isPrimary := false
		if image.IsPrimary == 1 {
			isPrimary = true
			formatter.ImageURL = image.FileName
		}
		formatterImage.IsPrimary = isPrimary

		formatterImages = append(formatterImages, formatterImage)
	}

	formatter.Images = formatterImages


	return formatter
}