package transaction

import "time"

type CampaignTransactionFormatter struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Amount int `json:"amount"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func FormatCampaignTransaction(transaction Transaction) CampaignTransactionFormatter {
	formatter := CampaignTransactionFormatter{
		ID: transaction.ID,
		Name: transaction.User.Name,
		Amount: transaction.Amount,
		CreatedAt: transaction.CreatedAt,
		UpdatedAt: transaction.UpdatedAt,
	}

	return formatter
}

func FormatCampaignTransactions(transactions []Transaction) []CampaignTransactionFormatter {
	if len(transactions) == 0 {
		return []CampaignTransactionFormatter{}
	}

	formatterTransactions := []CampaignTransactionFormatter{}
	for _, transaction := range transactions {
		formatterTransaction := FormatCampaignTransaction(transaction)

		formatterTransactions = append(formatterTransactions, formatterTransaction)
	}

	return formatterTransactions
}

type UserTransactionFormatter struct {
	ID int `json:"id"`
	Amount int `json:"amount"`
	Status string `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	Campaign UserCampaignTransactionFormatter `json:"campaign"`
}

type UserCampaignTransactionFormatter struct {
	Name string `json:"name"`
	ImageURL string `json:"image_url"`
}

func FormatUserTransaction(transaction Transaction) UserTransactionFormatter {
	formatter := UserTransactionFormatter{
		ID: transaction.ID,
		Amount: transaction.Amount,
		Status: transaction.Status,
		CreatedAt: transaction.CreatedAt,
		Campaign:UserCampaignTransactionFormatter{
			Name: transaction.Campaign.Name,
			ImageURL: "",
		},
	}

	if len(transaction.Campaign.CampaignImages) != 0 {
		formatter.Campaign.ImageURL = transaction.Campaign.CampaignImages[0].FileName
	}
	return formatter
}

func FormatUserTransactions(transactions []Transaction) []UserTransactionFormatter {
	userTransactions := []UserTransactionFormatter{}

	for _, transaction := range transactions {
		userTransactions = append(userTransactions, FormatUserTransaction(transaction))
	}
	return userTransactions
}

type TransactionFormatter struct {
	ID int `json:"id"`
	CampaignID int `json:"campaign_id"`
	UserID int `json:"user_id"`
	Amount int `json:"amount"`
	Status string `json:"status"`
	Code string `json:"code"`
	PaymentURL string `json:"payment_url"`
}

func FormatTransaction(transaction Transaction) TransactionFormatter {
	formatter := TransactionFormatter{
		ID: transaction.ID,
		CampaignID: transaction.CampaignID,
		UserID: transaction.UserID,
		Amount: transaction.Amount,
		Status: transaction.Status,
		Code: transaction.Code,
		PaymentURL: transaction.PaymentURL,
	}
	return formatter
}
